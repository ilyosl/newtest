<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'static/css/test/pages_style.css',
        'static/css/test/form_style.css',
        // 'static/css/test/modul_style.css',
        'static/css/jquery.countdown.css',
    ];
    public $js = [
        'static/css/test/main_custom.js',
        // 'static/css/test/timer.js',
        'static/js/jquery.plugin.min.js',
        'static/js/jquery.countdown.js',
        'static/js/jquery.countdown-ru.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}

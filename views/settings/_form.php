<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Settings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'test_time')->textInput() ?>

    <?= $form->field($model, 'count_question')->textInput() ?>

    <?= $form->field($model, 'limit_date_link')->textInput() ?>
    <?= $form->field($model, 'q_lang')->dropDownlist([
    	'ru' => 'Русский',
    	'en' => 'English'
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

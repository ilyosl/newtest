<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'TEST';
?>
<div class="site-index">

    <div class="jumbotron">
    		<?php if($user->created_at >= time()): ?>
    			<h3>Уважаемый кандидат!<br>
Приветствуем ваше желание пройти тест.<br>
Желаем удачи.</h3>
			 	<?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
    			 	<div class="form-group">
                        <?= Html::submitButton('Начать тестирование', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>
			<?php else: ?>
	            <div class="row">
	                <h3>Ссылка не активна. Вы не можете участвовать в тестировании</h3>
	            </div>
        	<?php endif;?>
    </div>
</div>
<style type="text/css">
    .centerBlock{
        float: none;
        margin: 0 auto;
    }
</style>
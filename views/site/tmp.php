<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
//use yii\captcha\Captcha;
$this->title = 'TEST';
?>
<div class="site-index">

    <div class="jumbotron">
        <div class="row">
            <?= date('d.m.Y H:i:s', 1560807120); ?>
        </div>
        <div class="row">
            <div class="col-md-6 centerBlock">
                 <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>


                    <?= $form->field($model, 'email') ?>


                    <?php /* $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                        'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                    ])*/ ?>

                    <div class="form-group">
                        <?= Html::submitButton('Send test link', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
       
    </div>
</div>
<style type="text/css">
    .centerBlock{
        float: none;
        margin: 0 auto;
    }
</style>
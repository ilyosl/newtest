<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$time = $settings->test_time*60;
$scriptchangeblog = <<< JS
		/*$('#test_timer').countDown({
            targetOffset: {
              'day': 0,
              'month': 0,
              'year': 0,
              'hour': 0,
              'min': 0,
              'sec': $time,
            },
            omitWeeks: true,
                        
            // startTime: '01:12:12:00';
            // onComplete function
            onComplete: function() {
              $("#test-form").submit();
            }
        });*/
        var austDay = new Date();
		austDay.setSeconds(austDay.getSeconds() + $time); 
		
		$('#defaultCountdown').countdown({until: austDay,  
	    onExpiry: liftOff, onTick: watchCountdown});

		function liftOff() { 
		    $("#test-form").submit(); 
		} 
		 
		function watchCountdown(periods) {
			// console.log(periods);
		    /*$('#year').text('Just ' + periods[5] + ' minutes and ' + 
		        periods[6] + ' seconds to go');*/ 
		}
JS;
$this->registerJs($scriptchangeblog, yii\web\View::POS_READY);
?>

<div class="row">
	<h2>Участник <?= $user->email?> </h2>
	<div class="col-md-2">
		<!-- <div class="timer_block" id="test_timer" align="center">
			        <div id="timer_fixer">
			          <div class="dash hours_dash">
			            <span class="dash_title">Час</span> 
			            <div class="digit">0</div>
			            <div class="digit">0</div>
			          </div>
		
			          <div class="dash minutes_dash">
			            <span class="dash_title">Мин</span>
			            <div class="digit">0</div>
			            <div class="digit">0</div>
			          </div>
		
			          <div class="dash seconds_dash">
			            <span class="dash_title">Сек</span> 
			            <div class="digit">0</div>
			            <div class="digit">0</div>
			          </div>
		
			        </div>
		      	</div>	 -->
		<div id="defaultCountdown" style="min-height: 57px;"></div>
	</div>
	
	<?php $form = ActiveForm::begin([ 
		'action'=>\yii\helpers\Url::to(['/site/result']), 
		'options'=>['class'=>'form-horizontal'],
		'id'=>'test-form'
	]); ?>
			<input type="hidden" name="user" value="<?= $user->email?>">
			<input type="hidden" id="cq" value="<?= count($questions)?>">
			<div class="box visible" style="display: block;">
										      <!-- math tab -->
		      	<div class="test_tabs">
		      		<h2 class="test_navi">
		      			Вопрос 1
			        </h2>
			        <!-- test navigator -->
			        <hr class="gray_line">
					<?php $i = 0; foreach($questions as $test):?>
				        <!-- test box -->
				        <div class="test_box  <?= ($i==0)?'visible':''?> "  id="a<?php echo $test->id?>" data-p=<?= $i+1; ?>>
				          <!-- test title -->
			          		<h3 class="test_title">
					          	<input class="result_id" data-question = '<?= $test->id?>' type="hidden" name='results[]' data-ch=<?=$i+1;?> value="">
					            <!-- <span></span> -->
					            <div class="container">
					            	<?= $test->newTitle?>
					            	<span style="float: right; font-weight: bold;">Сложность <?= $test->complexity ?></span>
					            </div>
				          	</h3>
				          <!-- test variants -->
				          <ul class="test_variants">
				          <?php $k = "A"; foreach($test->answers as $a):?>
				            <li><a href="#" data-option="<?php echo $a->id;?>"><div><?= $k?></div><span><?= $a->newTitle?></span></a></li>
				           <?php $k++; endforeach;?>
				          </ul>
				        </div>
				        <!-- test box end -->
			    	<?php $i++; endforeach;?>
		      	</div>
			      <!-- math tab end -->
				<div align="center" class="login_button" style="margin-top: 106px;">
					<a class="button pq" data-id='1' style="display:none"><span class="glyphicon glyphicon-chevron-left"></span> Вернуться</a>
					<a class="button nq" data-id='1' style="margin-right:22px">Продолжить <span class="glyphicon glyphicon-chevron-right"></span></i> </a>
			      	
			    </div>
			    <div style="clear: both;">
			    	
			    </div>
			    <div align="center" class="login_button" style="margin-top: 25px;" >
			      <input type="submit" class="button" value="Завершить тест">
			      		
			    </div>
		 	</div>
	<?php ActiveForm::end(); ?>
</div>
<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Тест',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Главная', 'url' => ['/site/index'],'visible'=>!Yii::$app->user->isGuest],
            ['label' => 'Добавить тесты', 'url' => ['/questions'],'visible'=>!Yii::$app->user->isGuest],
            ['label' => 'Участники', 'url' => ['/members'],'visible'=>!Yii::$app->user->isGuest],
            ['label' => 'Настройка', 'url' => ['/settings'],'visible'=>!Yii::$app->user->isGuest],
            Yii::$app->user->isGuest ? (
                ['label' => 'Главная', 'url' => ['/site/index']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Выход (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; TEST <?= date('Y') ?></p>

        <p class="pull-right">Powered by coding team</p>
    </div>
</footer>

<?php $this->endBody() ?>
<script type="text/javascript">
    $(document).ready(function(){
        $('.modal-btn').on('click', function() {
            $('#modal').modal('show')
                .find('#modal-content')
                .load($(this).attr('data-target'));
        });
        
    })
</script>
<script type="text/javascript">
    $('#sendchecked').on('click', function (event) {
       event.preventDefault();
        if (confirm('Вы действительно хотите отправить')) {
           
            
           var url = $(this).attr('href');
           var keys = $('#w0').yiiGridView('getSelectedRows');
            $.ajax({
                url: url,
                type: "POST",
                // dataType: "json",
                data: {keylist: keys},
                success:function(msg){
                    if(msg.status == 'success')
                        $("#w0").yiiGridView("applyFilter");
                    if(msg.status == 'notsend')
                        alert('ошибка на отправке');
                    if(msg.status=='wrong')
                        alert('вы не выбрали');
                }
            });
        } else {
            alert('Why did you press cancel? You should have confirmed');
        }
       //console.log(keys); 
    });
</script>
</body>
</html>
<?php $this->endPage() ?>

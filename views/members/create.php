<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Candidates */

$this->title = 'Добавления участника';
$this->params['breadcrumbs'][] = ['label' => 'Candidates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="candidates-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php


?>
<div class="row">
	<div class="col-md-12">
		<?php if($model): ?>
		<div class="col-md-12">
			<div class="portlet box blue">
				<div class="portlet-title">
					<div class="caption">
						Результаты теста
					</div>
					<div class="portlet-body">
						<table class="table table-bordered">
					        <thead>
					          <tr>
					            <td>№</td>
					            <td align="center" >Правильных ответов</td>
					            <td align="center" >Неправильных ответов</td>
					            <td align="center" >Балл</td>
					            <td align="center" >Дата сдачи</td>
					          </tr>
					        </thead>
					        <tbody>
					        	<?php $i=1; foreach($model as $result): ?>
					        		<tr>
					        			<td><?= $i; ?></td>
					        			<td><?= $result->true_answer; ?></td>
					        			<td><?= $result->wrong_answer; ?></td>
					        			<td><?= $result->result;?>  %</td>
					        			<td><?= date('d.m.Y H:i',$result->created_at); ?></td>
					        		</tr>
					        	<?php $i++; endforeach;?>
					        </tbody>
					    </table>
					</div>
				</div>
			</div>
		</div>
		<?php else:?>
			<h3>Нет ответов</h3>
		<?php endif; ?>
	</div>
</div>
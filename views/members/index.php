<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CandidatesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Учатники';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="candidates-index">
    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Отправить линк на выбранных', ['send-link'], ['class' => 'btn btn-success','id'=>'sendchecked']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\CheckboxColumn',
                'checkboxOptions' => function ($model, $key, $index, $column) {
                    if ($model->available_count_test == 0) {
                         return ['style' => ['display' => 'none']];
                    }
                    // OR ['disabled' => true]
                },
                /*'visible' => function ($data){
                    return ($data->available_count_test > 0) ? true : false ;
                }*/
            ],

            'id',
            'email:email',
            [
                'attribute'=>'available_count_test',
                'format' => 'raw',
                'value' => function ($data){
                    return ($data->available_count_test == 0) ? '<span class="label label-danger">Не возможна отправить ссылку</span>' : '';
                }
            ],
            [
                'attribute'=>'created_at',
                'format' =>  ['date', 'HH:m dd.MM.Y'],
                'headerOptions' => ['width' => '200'],
            ],
            [
                'attribute'=>'status',
                'format' => 'raw',
                'value' => function($data){
                    return $data->statusLink();
                }
            ],
            'link_test',
            [
                'label'=>'Статус теста',
                'format' => 'raw',
                'value' => function($data){
                    if($data->statusTest()){
                        return $data->statusTest();
                    }else
                        return '<span class="label label-danger">Участник не начел теста</span>';
                }
            ],
            [
                'label'=>'Результаты',
                'format' => 'raw',
                'value' => function($data){
                    return $data->resultView();
                }
            ],
            //'status',
            //'result',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
<?php
    yii\bootstrap\Modal::begin([
        // 'header' => 'Просмотр заказа',
        'id' => 'modal',
        'size' => 'modal-lg',
    ]);
    ?>
    <div id='modal-content'>идёт загрузка...</div>
<?php yii\bootstrap\Modal::end(); ?>
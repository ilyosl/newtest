<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<h4>Вопрос <?= $question->title ?></h4>
<?php $form = ActiveForm::begin(); ?>
	<?php $i=1; foreach($answers as $answer): ?>
		<div class="form-group">
			<label>
				<input type="checkbox" name="AnswerForm[is_true][<?= $answer->id ?>]" value="1" <?= ($answer->is_true)?'checked':'' ?> >Правилные
			</label>
		</div>
		
		<ul class="nav nav-tabs">
	      <li class="active"><a href="#titleru<?= $i?>" data-toggle="tab">ответ на русском</a></li>
	      <li><a href="#titleuz<?= $i?>" data-toggle="tab">ответ на английском</a></li>
	    </ul>
	     <div class="tab-content">
	        <div class="tab-pane active" id="titleru<?= $i?>">
				<?= $form->field($model, 'answers['.$answer->id.']')->textarea(['rows' => 6, 'value'=>$answer->answers]);?>
			 </div>
	        <div  class="tab-pane" id="titleuz<?= $i?>">
	            <?= $form->field($model, 'answers_en['.$answer->id.']')->textarea(['rows' => 6,'value'=>$answer->answers_en])->label(false) ?>
	        </div>
	    </div>
	<?php $i++; endforeach;?>

  <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end(); ?>
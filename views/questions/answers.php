<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<h4>Вопрос <?= $question->title ?></h4>
<?php $form = ActiveForm::begin(); ?>
	<?php for($i=1; $i<=$question->count_answers; $i++): ?>
		<div class="form-group">
			<label>
				<input type="checkbox" name="AnswerForm[is_true][<?= $i-1 ?>]" value="1">Правилные
			</label>
		</div>
		
		
		<ul class="nav nav-tabs">
	      <li class="active"><a href="#titleru<?= $i?>" data-toggle="tab">ответ на русском</a></li>
	      <li><a href="#titleuz<?= $i?>" data-toggle="tab">ответ на английском</a></li>
	    </ul>
	     <div class="tab-content">
	        <div class="tab-pane active" id="titleru<?= $i?>">
	            <?= $form->field($model, 'answers[]')->textarea(['rows' => 6])->label(false) ?>
	        </div>
	        <div  class="tab-pane" id="titleuz<?= $i?>">
	            <?= $form->field($model, 'answers_en[]')->textarea(['rows' => 6])->label(false) ?>
	        </div>
	    </div>
	<?php endfor;?>

  <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end(); ?>
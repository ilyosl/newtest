<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Вопросы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questions-index">

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'title:ntext',
            'complexity',
            'created_at:date',
            [
                'label'=>'Ответы',
                'format' => 'raw',
                'value' => function($data){
                    return $data->answersView();
                }
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
<?php
    yii\bootstrap\Modal::begin([
        // 'header' => 'Просмотр заказа',
        'id' => 'modal',
        'size' => 'modal-lg',
    ]);
    ?>
    <div id='modal-content'>идёт загрузка...</div>
<?php yii\bootstrap\Modal::end(); ?>
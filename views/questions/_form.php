<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Questions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="questions-form">

    <?php $form = ActiveForm::begin(); ?>
	<ul class="nav nav-tabs">
      <li class="active"><a href="#titleru" data-toggle="tab">Вопрос на русском</a></li>
      <li><a href="#titleuz" data-toggle="tab">Вопрос на английском</a></li>
    </ul>
     <div class="tab-content">
        <div class="tab-pane active" id="titleru">
            <?= $form->field($model, 'title')->textarea(['rows' => 6])->label(false) ?>
        </div>
        <div  class="tab-pane" id="titleuz">
            <?= $form->field($model, 'title_en')->textarea(['rows' => 6])->label(false) ?>
        </div>
    </div>
    
	<div class="row">
		<div class="col-md-8">
			<div class="col-md-6">
			<?= $form->field($model, 'complexity')->textInput() ?>		
			</div>
			<div class="col-md-6">
				<?= $form->field($model, 'count_answers')->textInput() ?>		
			</div>	
		</div>
		
	</div>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

namespace app\models;

use Yii;
use app\models\Checkingstarttest;
/**
 * This is the model class for table "candidates".
 *
 * @property int $id
 * @property string $email
 * @property int $available_count_test
 * @property int $created_at
 * @property string $link_test
 * @property int $status
 */
class Candidates extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'candidates';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email','available_count_test'],'required'],
            [['available_count_test', 'created_at', 'status','result'], 'integer'],
            [['email'], 'string', 'max' => 65],
            [['link_test'], 'string', 'max' => 255],
            [['email'],'email'],
            [['email'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'available_count_test' => 'количество прохождений',
            'created_at' => 'Дата истечения линка',
            'link_test' => 'Линк',
            'status' => 'Линк на тест',
        ];
    }
    public function getResults()
    {
        return $this->hasMany(TestResult::className(), ['member_id' => 'id']);
    }
    public function resultView(){

            return "<a href='#' class='modal-btn' data-target=".yii\helpers\Url::to(['results-view','id'=>$this->id]).">Просмотр резултатов</a>";
        
    }
    public function statusTest(){
        $cTest = Checkingstarttest::find()->where(['member_id'=>$this->id])->one();
        if($cTest){
            $html = '<p>Начал теста <span class="label label-warning">'.date('d.m.Y H:i',$cTest->start_time).'</span></p>';
            if($cTest->end_time == 0) 
                $html .='<p><span class="label label-danger">не завершил теста</span></p>';
            else 
                $html .='<p>завершил теста <span class="label label-success">'.date('d.m.Y H:i',$cTest->end_time).'</span></p>';
            return $html;
        }else
         return 0;
    }
    public function statusLink(){
        switch ($this->status) {
            case 1:
                return "<span class='label label-warning'>Link not send</span>";
                break;
            case 2:
                return "<span class='label label-success'>Link send</span>";
                break;
            default:
                return "<span class='label label-danger'>not defined</span>";
                break;
        };
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "test_result".
 *
 * @property string $id
 * @property int $member_id
 * @property int $result
 * @property int $created_at
 * @property int $true_answer
 * @property int $wrong_answer
 */
class TestResult extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'test_result';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            
            [['member_id', 'result', 'created_at', 'true_answer', 'wrong_answer'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'result' => 'Result',
            'created_at' => 'Created At',
            'true_answer' => 'True Answer',
            'wrong_answer' => 'Wrong Answer',
        ];
    }
    public function beforeSave($insert){
        if(!$this->created_at)
            $this->created_at = time();

        return parent::beforeSave($insert);
    }
}

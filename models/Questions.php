<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
use app\models\Answers;
use app\models\Settings;
/**
 * This is the model class for table "questions".
 *
 * @property int $id
 * @property string $title
 * @property int $complexity
 * @property int $created_at
 * @property int $status
 */
class Questions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $newTitle;
    public static function tableName()
    {
        return 'questions';
    }
    public function afterfind(){
        $lang = Settings::find()->one()->q_lang;
        if($lang == 'en' && $this->title_en){
            $this->newTitle = $this->title_en;
        }else{
            $this->newTitle = $this->title;
        }
        
         
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title','complexity','count_answers'],'required'],
            [['title','title_en'], 'string'],
            [['complexity', 'created_at', 'status','count_answers'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Вопрос',
            'title_en' => 'Вопрос на английском',
            'complexity' => 'Сложност',
            'created_at' => 'дата создание',
            'status' => 'Статус',
            'count_answers'=> 'кол. ответов'
        ];
    }
    public function beforeSave($insert){
        if(!$this->created_at)
            $this->created_at = time();

        return parent::beforeSave($insert);
    }
    public function getAnswers()
    {
        return $this->hasMany(Answers::className(), ['question_id' => 'id']);
    }
    public function answersView(){
        if(count($this->answers) == 0){
            $countAnswers = (isset($this->answers)) ? count($this->answers) : 0;
            return "добавлено ответов ".$countAnswers." <a href='#' class='modal-btn' data-target=".Url::to(['answer','q'=>$this->id]).">Добавить</a>";
        }else{
            return "Все ответы добавлено <a href='#' class='modal-btn' data-target=".Url::to(['answers-view','id'=>$this->id]).">Просмотр</a>";
        }
    }
    public function getTrueAnswers($id)
    {
        foreach ($this->answers as $value) {
            if($value->id == $id && $value->is_true > 0)
                return true;
        }
       /* $trueAnswers = Answers::find()->where('id=:aId',[':aId'=>$id])->one();
        if($trueAnswers->is_true > 0)
            return true;
        else */
        return false;
    }
}

<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class AnswerForm extends Model
{
    public $answers;
    public $answers_en;
    public $is_true;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['answers'], 'required'],
            // email has to be a valid email address
            ['is_true', 'integer'],
            [['answers','answers_en'],'string']
            // verifyCode needs to be entered correctly
            // ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'is_true' => 'Правилные',
            'answers' => 'ответ',
        ];
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "checkingstarttest".
 *
 * @property int $id
 * @property int $member_id
 * @property int $start_time
 * @property int $end_time
 */
class Checkingstarttest extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'checkingStartTest';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['member_id', 'start_time', 'end_time'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'start_time' => 'Start Time',
            'end_time' => 'End Time',
        ];
    }
}

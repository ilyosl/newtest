<?php

namespace app\models;

use Yii;
use app\models\Settings;
/**
 * This is the model class for table "answers".
 *
 * @property int $id
 * @property string $answers
 * @property int $is_true
 * @property int $question_id
 */
class Answers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $newTitle;
    public static function tableName()
    {
        return 'answers';
    }
    public function afterfind(){
        $lang = Settings::find()->one()->q_lang;
        if($lang == 'en' && $this->answers_en){
            $this->newTitle = $this->answers_en;
        }else{
            $this->newTitle = $this->answers;
        }
        
         
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['answers'], 'required'],
            [['is_true', 'question_id'], 'integer'],
            [['answers','answers_en'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'answers' => 'ответ',
            'is_true' => 'Правилные',
            'question_id' => 'Question ID',
        ];
    }
}

<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\Url;
use app\models\Candidates;
use app\models\Settings;
/**
 * ContactForm is the model behind the contact form.
 */
class SubscripeForm extends Model
{
    public $email;
    public $verifyCode;
    public $subject = 'Сдача теста';
    public $body;
    public $name = 'Участник';

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['email'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            // ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Каптча',
            'email' => 'Эл. почта',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function sendLink($link){
        if ($this->validate()) {
            
            $this->body = '<a href ="'.Yii::$app->request->hostInfo.Url::to(['/site/start-test','candidate'=>$link]).'">Линк на тест</a>';
            try {
                Yii::$app->mailer->compose()
                    ->setTo($this->email)
                    ->setFrom(['ilsweb@mail.ru' => 'Admin'])
                    ->setReplyTo([$this->email => $this->name])
                    ->setSubject($this->subject)
                    ->setHtmlBody($this->body)
                    ->send();
            } catch (\Exception $e) {
                return false;
            }
           
            return true;
        }
        else
            return false;

    }
    public function subscripe()
    {
        $exp_date = Settings::find()->one()->limit_date_link;
        $candidate = Candidates::find()->where(['email'=>$this->email])->one();
        if($candidate && ( $candidate->created_at > time() || $candidate->available_count_test == 0)){

            return false;
        }
        if ($this->validate()) {
            $hashlink = md5($this->email);
            $this->body = '<a href ="'.Yii::$app->request->hostInfo.Url::to(['/site/start-test','candidate'=>$hashlink]).'">Линк на тест</a>';
            Yii::$app->mailer->compose()
                ->setTo($this->email)
                ->setFrom(['ilsweb@mail.ru' => 'Admin'])
                ->setReplyTo([$this->email => $this->name])
                ->setSubject($this->subject)
                ->setHtmlBody($this->body)
                ->send();
            if(empty($candidate)){
                $candidate = new Candidates();
                $candidate->email = $this->email;
                $candidate->available_count_test  = 1;
                $candidate->created_at = strtotime( date('d.m.Y H:i').'+ '.$exp_date.' hours');
                $candidate->link_test = $hashlink;
                $candidate->save();
            }else{
                $candidate->available_count_test -=1;
                $candidate->created_at = strtotime( date('d.m.Y H:i').'+ '.$exp_date.' hours');
                $candidate->save(false);
            }
            return true;
        }
        return false;
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property int $id
 * @property int $test_time
 * @property int $count_question
 * @property int $count_answers
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['test_time', 'count_question', 'count_answers','limit_date_link'], 'integer'],
            ['q_lang','string'],
            [['count_question', 'limit_date_link'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'test_time' => 'Время теста(на минутах)',
            'count_question' => 'Кол. Вопросов',
            'count_answers' => 'Кол. ответов',
            'q_lang' =>'Язык вопроса',
            'limit_date_link' => 'Дата истечения теста(в часах, напр: 24)',
        ];
    }
}

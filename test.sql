-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июн 19 2019 г., 13:17
-- Версия сервера: 5.7.26-0ubuntu0.16.04.1
-- Версия PHP: 7.0.33-0ubuntu0.16.04.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `admin_ntest`
--

-- --------------------------------------------------------

--
-- Структура таблицы `answers`
--

CREATE TABLE `answers` (
  `id` int(11) NOT NULL,
  `answers` varchar(255) NOT NULL,
  `is_true` tinyint(2) DEFAULT '0',
  `question_id` int(11) NOT NULL,
  `answers_en` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `answers`
--

INSERT INTO `answers` (`id`, `answers`, `is_true`, `question_id`, `answers_en`) VALUES
(1, '1', 1, 2, NULL),
(2, '2', 0, 2, NULL),
(3, '3', 0, 2, NULL),
(4, '4', 0, 2, NULL),
(5, '5', 0, 2, NULL),
(6, 'оливье222', 0, 3, 'asdfas'),
(7, 'винегрет', 0, 3, ''),
(8, 'мимоза', 3, 3, ''),
(9, 'селёдка под шубой', 0, 3, ''),
(10, 'рукава', 1, 4, NULL),
(11, 'воротника', 0, 4, NULL),
(12, 'лацкана', 0, 4, NULL),
(13, 'манжеты', 0, 4, NULL),
(14, '•	на первом', 0, 5, '•	на первом en'),
(15, '•	на втором', 2, 5, '•	на втором en'),
(16, '•	на третьем', 0, 5, '•	на третьем en'),
(17, '•	на четвёртом', 0, 5, '•	на четвёртом en'),
(18, '•	на пятом', 0, 5, '•	на пятом en'),
(19, '•	на шестом ', 0, 5, '•	на шестом  en'),
(20, '•	на седьмом', 0, 5, '•	на седьмом en'),
(21, '•	на восьмом', 0, 5, '•	на восьмом en'),
(22, '•	дельфин', 1, 6, '•	дельфин en'),
(23, '•	медведь', 0, 6, '•	медведь en'),
(24, '•	попугай', 1, 6, '•	попугай en'),
(25, '•	крокодил', 0, 6, '•	крокодил en'),
(26, '•	на шею ', 1, 7, '•	на шею en'),
(27, '•	на пальцы', 0, 7, '•	на пальцы en'),
(28, '•	на уши', 1, 7, '•	на уши en'),
(29, '•	на волосы', 0, 7, '•	на волосы en'),
(30, '•	на зубы', 1, 7, '•	на зубы en'),
(31, 'совковая', 1, 8, ''),
(32, 'граблевая', 0, 8, ''),
(33, 'тяпковая', 0, 8, ''),
(34, 'совковая', 1, 9, ''),
(35, 'граблевая', 0, 9, ''),
(36, 'тяпковая', 0, 9, '');

-- --------------------------------------------------------

--
-- Структура таблицы `candidates`
--

CREATE TABLE `candidates` (
  `id` int(11) NOT NULL,
  `email` varchar(65) DEFAULT NULL,
  `available_count_test` tinyint(3) DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `link_test` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `result` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `candidates`
--

INSERT INTO `candidates` (`id`, `email`, `available_count_test`, `created_at`, `link_test`, `status`, `result`) VALUES
(17, 'webfoza@gmail.com', 0, 1561026480, '74ae2867bae35be23bfb576a2b9d2b8f', 2, 0),
(22, 'ilyosl@mail.ru', 1, 1560978000, 'c37be1a364d90c367dfa6e7821dde38a', 2, 0),
(26, 'uzb.php.job@gmail.com', 8, 1561026300, 'b0e883c7dd55ed20a3ad8859acf5f0b9', 2, 66);

-- --------------------------------------------------------

--
-- Структура таблицы `checkingStartTest`
--

CREATE TABLE `checkingStartTest` (
  `id` int(11) NOT NULL,
  `member_id` int(11) DEFAULT NULL,
  `start_time` int(11) DEFAULT NULL,
  `end_time` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `checkingStartTest`
--

INSERT INTO `checkingStartTest` (`id`, `member_id`, `start_time`, `end_time`) VALUES
(1, 22, 1560948896, 1560949667),
(2, 26, 1560950104, 1560950130);

-- --------------------------------------------------------

--
-- Структура таблицы `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `title` text,
  `complexity` tinyint(2) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `count_answers` tinyint(2) DEFAULT '4',
  `title_en` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `questions`
--

INSERT INTO `questions` (`id`, `title`, `complexity`, `created_at`, `status`, `count_answers`, `title_en`) VALUES
(2, 'Как заканчивается присказка: "Мы и сами с..."?', 1, 1560702183, 1, -10, ''),
(3, 'Как в обиходе называют растение, которое часто дарят женщинам к празднику 8 марта?', 2, 1560706649, 1, -6, NULL),
(4, 'С помощью чего пасссажиры часто попадают в самолет?', 1, 1560712807, 1, -6, NULL),
(5, 'На каком курсе школы Хогвартс учился Гарри Поттер, когда раскрыл секрет Тайной комнаты?', 6, 1560881545, 1, 8, NULL),
(6, 'Кто такой ара?', 4, 1560929696, 1, 4, 'Кто такой ара? en'),
(7, 'На что надевают брекеты?', 3, 1560936522, 1, 5, 'На что надевают брекеты? en'),
(9, 'Какая бывает лопата?', 1, 1560949916, 1, 3, '');

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `test_time` tinyint(3) DEFAULT NULL,
  `count_question` tinyint(3) NOT NULL,
  `count_answers` tinyint(2) DEFAULT NULL,
  `limit_date_link` tinyint(3) DEFAULT '24',
  `q_lang` varchar(3) DEFAULT 'ru'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`id`, `test_time`, `count_question`, `count_answers`, `limit_date_link`, `q_lang`) VALUES
(1, 5, 10, NULL, 24, 'ru');

-- --------------------------------------------------------

--
-- Структура таблицы `test_result`
--

CREATE TABLE `test_result` (
  `id` int(11) NOT NULL,
  `member_id` int(11) DEFAULT NULL,
  `result` tinyint(4) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `true_answer` tinyint(4) DEFAULT NULL,
  `wrong_answer` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `test_result`
--

INSERT INTO `test_result` (`id`, `member_id`, `result`, `created_at`, `true_answer`, `wrong_answer`) VALUES
(21, 22, 50, 1560889699, 2, 1),
(22, 22, 50, 1560891645, 2, 1),
(23, 22, 50, 1560891685, 2, 1),
(24, 22, 25, 1560891741, 1, 2),
(25, 22, 0, 1560938682, 0, 6),
(26, 22, 0, 1560940759, 0, 6),
(27, 26, 0, 1560940827, 0, 6),
(28, 22, 0, 1560941150, 0, 6),
(29, 26, 0, 1560941293, 0, 6),
(30, 26, 0, 1560941471, 0, 6),
(31, 26, 0, 1560942919, 0, 6),
(32, 22, 0, 1560945670, 0, 6),
(33, 26, 0, 1560945942, 0, 6),
(34, 22, 0, 1560948925, 0, 6),
(35, 22, 0, 1560949667, 0, 6);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `candidates`
--
ALTER TABLE `candidates`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `checkingStartTest`
--
ALTER TABLE `checkingStartTest`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `test_result`
--
ALTER TABLE `test_result`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT для таблицы `candidates`
--
ALTER TABLE `candidates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT для таблицы `checkingStartTest`
--
ALTER TABLE `checkingStartTest`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `test_result`
--
ALTER TABLE `test_result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

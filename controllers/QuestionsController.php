<?php

namespace app\controllers;

use Yii;
use app\models\Questions;
use app\models\QuestionsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\Answers;
use app\models\AnswerForm;
use yii\base\Model;
/**
 * QuestionsController implements the CRUD actions for Questions model.
 */
class QuestionsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','view','create','update','delete','answer','answers-view'],
                'rules' => [
                    [
                        'actions' => ['index','view','create','update','delete','answer','answers-view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Questions models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new QuestionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionAnswer($q){
        $question = $this->findModel($q);
        $formAnswer = new AnswerForm;
        // validation checking count answers;
        if (Yii::$app->request->isPost) {
            $i = 1;
            $addActive = false;
            
            foreach ($_POST['AnswerForm']['answers'] as $key => $value) {
                if($value){
                    $model = new Answers;
                    $model->answers = $value;
                    $model->answers_en = $_POST['AnswerForm']['answers_en'][$key];
                    $model->is_true = (isset($_POST['AnswerForm']['is_true'][$key])) ? $_POST['AnswerForm']['is_true'][$key] : 0;;
                    $model->question_id = $question->id;
                    $model->save(false);
                    $addActive = true;
                }
                
            }
            if($addActive){
                $question->status = 1;
                $question->save(false);    
            }
            
            return $this->redirect(['index']);
        }
        return $this->renderAjax('answers',['model'=>$formAnswer,'question'=>$question]);
    }
    public function actionAnswersView($id){
        if($id){
            $question = $this->findModel($id);
            if($question){
                $answers = Answers::find()->where(['question_id'=>$question->id])->all();
                $answerForm = new AnswerForm;
                if (Yii::$app->request->isPost) {
                    $addActive = false;
                    $i = 0;

                    foreach ($_POST['AnswerForm']['answers'] as $key => $value) {
                        if($value){
                            $model = Answers::findOne($key);
                            $model->answers = $value;
                            $model->answers_en = $_POST['AnswerForm']['answers_en'][$key];
                            $model->is_true = (isset($_POST['AnswerForm']['is_true'][$key])) ? $_POST['AnswerForm']['is_true'][$key] : 0;
                            $model->question_id = $question->id;
                            $model->save(false);
                            $i++;
                            $addActive = true;
                        }
                        
                    }
                    if($addActive){
                        $question->status = 1;
                        $question->save(false);    
                    }
                    return $this->redirect(['index']);
                }
                return $this->renderAjax('answersView',['model'=>$answerForm,'question'=>$question, 'answers'=>$answers]);
            }
        }

    }
    /**
     * Displays a single Questions model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Questions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Questions();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Questions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Questions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Questions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Questions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Questions::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

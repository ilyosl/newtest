<?php

namespace app\controllers;

use Yii;
use app\models\TestResult;
use app\models\Candidates;
use app\models\CandidatesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\Settings;
use app\models\SubscripeForm;
use yii\web\Response;
/**
 * MembersController implements the CRUD actions for Candidates model.
 */
class MembersController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','view','create','update','delete','result','results-view'],
                'rules' => [
                    [
                        'actions' => ['index','view','create','update','delete','result','results-view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Candidates models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CandidatesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionResultsView($id){
        $user = $this->findModel($id);
        $results = TestResult::find()->where(['member_id'=>$id])->all();

        return $this->renderAjax('resultTest',['model'=>$results,'user'=>$user]);

    }

    /**
     * Displays a single Candidates model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Candidates model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Candidates();

        if ($model->load(Yii::$app->request->post())) {
            $exp_date = Settings::find()->one()->limit_date_link;
            $model->link_test = md5($model->email);
            $model->created_at = strtotime( date('d.m.Y H:i').'+ '.$exp_date.' hours');
            if($model->save())
                return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    public function actionSendLink(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        if(isset($_POST['keylist'])){
            foreach ($_POST['keylist'] as $key => $value) {
                $user = $this->findModel($value);
                if($user){
                    $model = new SubscripeForm();
                    $model->email = $user->email;
                    if($user->available_count_test >0 && $model->sendLink($user->link_test)){
                        $user->status = 2;
                        $user->available_count_test -= 1;
                        $user->save(false);
                        return ['status'=>'success'];
                    }
                    else
                        return ['status'=>'notsend'];
                }
                
            }
        }else{
            return['status'=>'wrong'];
        }
    }

    /**
     * Updates an existing Candidates model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $exp_date = Settings::find()->one()->limit_date_link;
            $model->link_test = md5($model->email);
            $model->created_at = strtotime( date('d.m.Y H:i').'+ '.$exp_date.' hours');
            if($model->save())
                return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Candidates model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Candidates model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Candidates the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Candidates::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\SubscripeForm;
use app\models\Candidates;
use app\models\Questions;
use app\models\TestForm;
use app\models\TestResult;
use app\models\Settings;
use app\models\Checkingstarttest;
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout','about'],
                'rules' => [
                    [
                        'actions' => ['logout','about'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new SubscripeForm();
        if(Yii::$app->request->isPost && $model->load(Yii::$app->request->post()) && $model->validate()){
            if($model->subscripe()){
                Yii::$app->session->setFlash('success', "Линк на тест отправлено"); 
            }else{
                Yii::$app->session->setFlash('error', "Ошибка!");
            }
        }
        return $this->render('index',['model'=>$model]);
    }
    public function actionStartTest($candidate = 0){
        if($candidate){
            $user = Candidates::find()->where(['link_test'=>$candidate])->one();
            $model = new TestForm();
            if(Yii::$app->request->isPost){

                $settings = Settings::find()->one();
                $checkingTest = Checkingstarttest::find()->where(['member_id'=>$user->id])->one();
                if(empty($checkingTest)){
                    $checkingTest = new Checkingstarttest();
                    $checkingTest->member_id = $user->id;
                    $checkingTest->start_time = time();
                    $checkingTest->end_time = 0;
                    $checkingTest->save();
                }else{
                    $checkingTest->start_time = time();
                    $checkingTest->end_time = 0;
                    $checkingTest->save();
                }
                $questions = Questions::find()->where(['status'=>1])->limit($settings->count_question)->all();
                return $this->render('test',['questions'=>$questions,'user'=>$user,'settings'=>$settings]);
            }else{
                return $this->render('testStart',['user'=>$user]);    
            }
            
        }else{
            return $this->redirect(['index']);
        }
    }
    public function actionResult(){
        if(isset($_POST['results']))
        {
            $session = Yii::$app->session;
            
            $resutls = $_POST['results'];
            $true = 0;
            $wrong = 0;
            $ball = 0;
            $maxBall = 0;
            foreach($resutls as $one)
            {
                $dvide = explode('-', $one);
                $question = Questions::findOne(intval($dvide[0]));
                if($question){
                    if(isset($dvide[1])){
                        if($question->getTrueAnswers(intval($dvide[1]))){
                            $true++;
                            $ball = $ball + $question->complexity;
                        }
                        else 
                            $wrong++;
                    }
                    else 
                        $wrong++;
                    $maxBall = $maxBall + $question->complexity;
                }else{
                    $wrong++;
                }
                
            }
            $res = ($ball > 0) ? ($ball*100)/($maxBall) : 0;
            $user = Candidates::find()->where(['email'=>$_POST['user']])->one();
            $user->result = $res;
            if($user->available_count_test == 0)
                $user->created_at = time();
            
            $user->save(false);
            $checkingTest = Checkingstarttest::find()->where(['member_id'=>$user->id])->one();
            $checkingTest->end_time = time();
            $checkingTest->save();

            $resultUser = new TestResult();
            $resultUser->member_id = $user->id;
            $resultUser->result = $res;
            $resultUser->true_answer = $true;
            $resultUser->wrong_answer = $wrong;
            $resultUser->save();
            $log = [];
            try {
                
                Yii::$app->mailer->compose()
                    ->setTo($user->email)
                    ->setFrom(['ilsweb@mail.ru' => 'Admin'])
                    ->setReplyTo([$user->email => 'Участник'])
                    ->setSubject('Результат теста')
                    ->setHtmlBody($this->getResultHtml($true,$wrong,$res, time()))
                    ->send();
            } catch (\Exception $e) {
                $log = $e;
            }
            // echo $true.' '.$wrong; die;
            return $this->render('test_result',['ball'=>$true,'maillog'=>$log]);
        }
    }
    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    public function getResultHtml($true = 0, $wrong = 0, $ball = 0, $resultDate){
        return '<table border = "1">
                    <thead>
                      <tr>
                        <td align="center" >Правильных ответов</td>
                        <td align="center" >Неправильных ответов</td>
                        <td align="center" >Балл</td>
                        <td align="center" >Дата сдачи</td>
                      </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>'.$true.'</td>
                            <td>'.$wrong.'</td>
                            <td> '.$ball.'%</td>
                            <td>'.date("d.m.Y H:i",$resultDate).'</td>
                        </tr>
                    </tbody>
                </table>';
    }
}
